#### This project is for the Devops bootcamp exercise for 
#### "Build Tools and Package Managers" 

##### First step:
- gradle build

##### Second step:
- Fixing AppTest.java compilation error by changing from string to boolean
- gradle test

##### Third step:
- gradle clean
- gradle build

##### Fourth step:
- under build/libs, run the jar file using the command: java -jar build-tools-exercise-1.0-SNAPSHOT.jar

##### Fifth step:
- modify application to accept two arguments and to log information about the arguments
- gradle build
- java -jar build/libs/build-tools-exercise-1.0-SNAPSHOT.jar arg1 arg2
